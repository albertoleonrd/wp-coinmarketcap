<?php

/*
Plugin Name: WP CoinMarketCap
Plugin URI:  https://roelmagdaleno.com/
Description: Get the latest cryptocurrency data.
Version:     1.0
Author:      Roel Magdaleno
Author URI:  https://roelmagdaleno.com/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

function wp_cmkcap_add_admin_bar_menu( $wp_admin_bar ) {
   $parent_node = array(
      'id'    => 'wp_cmkcap_admin_bar',
      'title' => 'Cryptocurrencies',
   );

   $wp_admin_bar->add_node( $parent_node );

   $convert_to            = 'MXN';
   $just_show             = 5;
   $coinmarketcap_api_url = 'https://api.coinmarketcap.com/v1/ticker/?convert=' . $convert_to . '&limit=' . $just_show;
   $args                  = array(
      'method' => 'GET',
   );
   $response              = wp_remote_request( $coinmarketcap_api_url, $args );
   $cryptocurrencies      = json_decode( wp_remote_retrieve_body( $response ), true );

   foreach ( $cryptocurrencies as $key => $cryptocurrency_data ) {
      $child_node = array(
         'id'    => 'cryptocurrency-' . $cryptocurrency_data['id'],
         'title' => $cryptocurrency_data['name'] . ' / ' . $cryptocurrency_data['price_mxn'] . ' MXN',
         'parent' => 'wp_cmkcap_admin_bar',
      );

      $wp_admin_bar->add_node( $child_node );
   }
}

add_action( 'admin_bar_menu', 'wp_cmkcap_add_admin_bar_menu', 999 );